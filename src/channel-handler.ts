import * as Discord from "discord.js";
import { calendar_v3 } from "googleapis";

export interface EventBucketEntry {
    calendarId: string;
    event: calendar_v3.Schema$Event;
}

export interface Game {
    pk: string;
    startTime: Date;
    announced: boolean;

    toString(): string;
}

export interface ChannelHandler<T extends Game> {
    calendarIds: string[];
    gamesForEventBucket: (entries: EventBucketEntry[]) => T[];
    announceGames: (channel: Discord.TextChannel, games: T[],
        warningMinutes: number, debug: boolean, pingRole: boolean) => void;
}
